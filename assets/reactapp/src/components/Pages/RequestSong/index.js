import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { Recap } from 'src/comp/misc/Scripts/Recaptcha';
import { Done } from 'src/comp/Pages/RequestSong/Done';
import { requestSong } from 'src/redux/Songs/action';
import _ from 'lodash';

class RequestSong extends Component {
  constructor(props) {
    super(props);
    this.verifyCallback = this.verifyCallback.bind(this);
    this.expiredCallback = this.expiredCallback.bind(this);
    this.resetState = this.resetState.bind(this);
  }

  componentWillMount() {
    this.resetState();
  }

  verifyCallback() {
    this.setState({ disabledAdd: false, recaptchaValidated: true });
  }

  expiredCallback() {
    this.setState({ recaptchaValidated: false });
  }

  resetState() {
    this.setState({
      recaptchaValidated: false,
      disabledAdd: true,
      Done: false,
      form: {
        title: '',
        artist: '',
        album: '',
        url: '',
        email: '',
      },
      titleError: '',
    });
  }

  request() {
    if (!this.state.recaptchaValidated) {
      this.setState({ disabledAdd: true });
      return;
    }

    const emptyTitle = _.isEmpty(this.state.form.title);

    (emptyTitle)
      ? this.setState({ titleError: 'ادخل اسم ترنيمة' })
      : this.setState({ titleError: '' });

    if (!emptyTitle) {
      this.props.requestSong(this.state.form);
      this.setState({ Done: true });
    }
  }

  handleInput(e, value) {
    const tempForm = {
      ...this.state.form,
      [e.target.name]: value,
    };
    this.setState({ form: tempForm });
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>اطلب ترنيمة</title>
          <meta name="description" content="كوردات ترنيمة مش معروفة؟ فريق بيت التسبيح يقدر يساعدك في عزفها" />
        </Helmet>
        <div className="form">
          {
                        !this.state.Done
                            ? (
                              <React.Fragment>
                                <div className="form-message">
                                  <h3>اطلب كوردات ترنيمة</h3>
                                  <p>تقدر تطلب كوردات ترنيمة تريدها و سيحاول فريق عمل بيت التسبيح على توفيرها في أسرع
                                        وقت.
                                  </p>
                                </div>
                                <Paper className="form-paper request-song">
                                  <TextField
                                    className="right-col"
                                    name="title"
                                    hintText="اسم الترنيمة*"
                                    errorText={this.state.titleError}
                                    onChange={(e, v) => {
                                            this.handleInput(e, v);
                                        }}
                                  />
                                  <TextField
                                    className="left-col"
                                    name="artist"
                                    hintText="اسم المرنم/الفريق"
                                    onChange={(e, v) => {
                                            this.handleInput(e, v);
                                        }}
                                  />
                                  <TextField
                                    className="right-col"
                                    name="album"
                                    hintText="اسم الشريط (إن وجد)"
                                    onChange={(e, v) => {
                                            this.handleInput(e, v);
                                        }}
                                  />
                                  <TextField
                                    className="left-col"
                                    name="link"
                                    hintText="رابط للترنيمة فيديو أو صوتي (إن وجد)"
                                    onChange={(e, v) => {
                                            this.handleInput(e, v);
                                        }}
                                  />
                                  <TextField
                                    className="right-col"
                                    name="email"
                                    hintText="بريدك الإلكتروني  (لمعرفة إضافتها)"
                                    onChange={(e, v) => {
                                            this.handleInput(e, v);
                                        }}
                                  />
                                </Paper>
                                <div className="save-button">
                                  <RaisedButton
                                    label="اطلب التـــرنيمة"
                                    primary={!this.state.disabledAdd}
                                    disableTouchRipple={this.state.disabledAdd}
                                    onClick={() => {
                                            this.request();
                                        }}
                                  />

                                </div>
                                <Recap verifyCallback={this.verifyCallback} expiredCallback={this.expiredCallback} />

                              </React.Fragment>)
                            : <Done />

                    }
        </div>
      </React.Fragment>);
  }
}

RequestSong.propTypes = {
  requestSong: PropTypes.func.isRequired,
};

export default connect(null, { requestSong })(RequestSong);
