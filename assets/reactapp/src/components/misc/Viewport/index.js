import { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setAppSize } from 'src/redux/UI/action';

export class Viewport extends Component {
  componentDidMount() {
    this.props.setAppSize(window.innerWidth);
  }
  render() {
    return null;
  }
}
Viewport.propTypes = {
  setAppSize: PropTypes.func.isRequired,
};

export default connect(null, { setAppSize })(Viewport);
