import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import createStore from 'src/redux';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';
import { createBrowserHistory as createHistory } from 'history'; // eslint-disable-line  import/no-extraneous-dependencies
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Header from 'src/comp/Header';
import HomePage from 'src/comp/Pages/HomePage';
import SongsIndex from 'src/comp/Pages/SongsIndex';
import Song from 'src/comp/Song';
import AddSong from 'src/comp/Pages/AddSong';
import RequestSong from 'src/comp/Pages/RequestSong';
import ContactUs from 'src/comp/Pages/ContactUs';
import SearchResult from 'src/comp/Pages/SearchResult';
import PrivacyPolicy from 'src/comp/Pages/PrivacyPolicy';
import ReactGA from 'react-ga';

const history = createHistory();
const store = createStore(history);

ReactGA.initialize('UA-51924183-3');

const App = () => (
  <div className="container">
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <MuiThemeProvider muiTheme={getMuiTheme(lightBaseTheme)}>
          <div className="appBody">
            <Header />
            <Switch>
              <Route exact path="/" component={HomePage} />
              <Route path="/songsIndex/:letter" component={SongsIndex} />
              <Route exact path="/request" component={RequestSong} />
              <Route exact path="/add-song" component={AddSong} />
              <Route exact path="/contact-us" component={ContactUs} />
              <Route path="/song/:songUrl" component={Song} />
              <Route path="/result" component={SearchResult} />
              <Route path="/privacypolicy" component={PrivacyPolicy} />
            </Switch>
          </div>

        </MuiThemeProvider>
      </ConnectedRouter>

    </Provider>
  </div>
);
export default App;

render(<App/>, document.getElementById('app')); //eslint-disable-line
