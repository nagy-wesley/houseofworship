import React from 'react';
import Viewport from 'src/comp/misc/Viewport';
import Navigation from 'src/comp/Nav';
import { Helmet } from 'react-helmet';
import HeadSearch from './HeadSearch';

const Header = () => (
  <React.Fragment>
    <Helmet defaultTitle="بيت التسبيح - كوردات ترانيم مسيحية" titleTemplate="%s - بيت التسبيح">
      <meta charSet="utf-8" />
      <meta name="description" content="بيت التسبيح موقع لكوردات الترانيم المسيحية و بيت راحة لمحبي التسبيح و العبادة" />
      <meta name="keywords" content="ترانيم, ترانيم مسيحية, ترانيم جديدة, ترانيم مسيحيه, ترانيم مسيحية قديمة, ترانيم الميلاد" />
    </Helmet>
    <Viewport />
    <Navigation />
    <div className="header">
      <h1>      بيت التسبيح      </h1>
      <HeadSearch />
    </div>
  </React.Fragment>);

export default Header;
