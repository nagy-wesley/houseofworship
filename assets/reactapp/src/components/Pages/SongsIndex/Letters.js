import _ from 'lodash';

export const lettersArray = [
  'أ',
  'ب',
  'ت',
  'ث',
  'ج',
  'ح',
  'خ',
  'د',
  'ذ',
  'ر',
  'ز',
  'س',
  'ش',
  'ص',
  'ض',
  'ع',
  'غ',
  'ف',
  'ق',
  'ك',
  'ل',
  'م',
  'ن',
  'ه',
  'و',
  'ي',
];

const alef = ['ا', 'أ', 'آ', 'إ'];

export const filterSongs = (letter, songsList) => {
  if (!_.includes(alef, letter)) {
    return _.filter(songsList, song => _.startsWith(song.title, letter));
  }

  const mapped = _.map(alef, a => _.filter(songsList, song => _.startsWith(song.title, a)));
  return _.flatten(mapped);
};

export default lettersArray;
