import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';
import { withStyles } from '@material-ui/core/styles';

class NoResults extends Component {
  goto(link) {
    return () => {
      this.context.router.history.push(link);
    };
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.noResultBase}>
        <h2>لم يتم العثور على نتائج</h2>
        <p>و لكن تستطيع ان تطلب من فريق العمل المساعدة على العثور على الكوردات!</p>
        <RaisedButton className={classes.requestButton} label="اطلب ترنيمة جديدة" primary onClick={this.goto('/request')} />
        <RaisedButton className={classes.gohomeButton} label="الرجوع الى الرئيسية" onClick={this.goto('/')} />
      </div>);
  }
}

NoResults.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.shape({ push: PropTypes.func.isRequired, replace: PropTypes.func.isRequired }).isRequired,
    staticContext: PropTypes.object,
  }).isRequired,
};


NoResults.propTypes = {
  classes: PropTypes.object.isRequired,
};

const styles = {
  noResultBase: {
    gridColumn: '2/3',
    gridRow: '4/5',
  },
  requestButton: {
    marginTop: '2%',
    marginRight: '5%',
  },
  gohomeButton: {
    marginTop: '2%',
    marginRight: '5%',
  },
};

export default withStyles(styles)(NoResults);
