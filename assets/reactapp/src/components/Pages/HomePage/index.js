import React from 'react';
import HomePageSongs from 'src/comp/Pages/HomePage/HomePageSongs';
import SCPlayer from 'src/comp/misc/SCPlayer';

const HomePage = () => (
  <React.Fragment>
    <div className="homepageContent">
      <HomePageSongs type="latest" />
      <HomePageSongs type="featured" />
    </div>
    <SCPlayer />
    <div className="footer">
      <a href="/privacypolicy">(سياسة الخصوصية)</a>
    </div>
  </React.Fragment>
);

export default HomePage;
