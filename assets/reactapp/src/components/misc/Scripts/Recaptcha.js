import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Recaptcha from 'react-google-recaptcha';

export class Recap extends Component {
  render() {
    const sitekey = '6LdH5FYUAAAAAEQYvVykJJ1YYE1vvchSlu9lW264';

    return (
      <Recaptcha
        className="recaptcha-widget"
        sitekey={sitekey}
        ref="recaptcha" // eslint-disable-line
        size="normal"
        onChange={this.props.verifyCallback}
        onExpired={this.props.expiredCallback}
      />);
  }
}

Recap.propTypes = {
  verifyCallback: PropTypes.func.isRequired,
  expiredCallback: PropTypes.func.isRequired,
};
