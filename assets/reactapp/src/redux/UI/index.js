import reducer from './reducer';

const initialState = {
  isDesktop: true,
  isMobile: false,
  isTab: false,
};

const uiReducer = (state = initialState, action) => (reducer[action.type]
  ? reducer[action.type](state, action)
  : state);

export default uiReducer;
