<?php

namespace App\Repository;

use App\Entity\Song;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Common\Cache\PredisCache;
use Predis\Client;

/**
 * @method Song|null find($id, $lockMode = null, $lockVersion = null)
 * @method Song|null findOneBy(array $criteria, array $orderBy = null)
 * @method Song[]    findAll()
 * @method Song[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SongRepository extends ServiceEntityRepository {

    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, Song::class);
    }

    public function getAllSongs(string $letter = null) {
        # init predis client
        $predis = new PredisCache(new Client());

        # define cache lifetime period as 1 week in seconds
        $cache_lifetime = 604800;

        $qb = $this->createQueryBuilder('s')
                ->select('s.title', 's.uniqueUrl');


        if ($letter) {
            if (!in_array($letter, ['ا', 'أ', 'آ', 'إ'])) {
                $qb->where($qb->expr()->like('s.title', $qb->expr()->literal($letter . '%')));
            } else {
                $qb->where($qb->expr()->like('s.title', $qb->expr()->literal('ا%')));
                $qb->orWhere($qb->expr()->like('s.title', $qb->expr()->literal('أ%')));
                $qb->orWhere($qb->expr()->like('s.title', $qb->expr()->literal('آ%')));
                $qb->orWhere($qb->expr()->like('s.title', $qb->expr()->literal('إ%')));
            }
        }

        return $qb->orderBy('s.title', 'ASC')
                        ->getQuery()
                        # pass predis object as driver
                        ->setResultCacheDriver($predis)
                        # set cache lifetime
                        ->setResultCacheLifetime($cache_lifetime)
                        ->getArrayResult();
    }

    public function saveSong(Song $song) {
        $em = $this->getEntityManager();
        $em->persist($song);
        $em->flush();
        $predis = new PredisCache(new Client());
        $predis->flushAll();
        return $song;
    }

    public function getSongByURL(string $url) {
        if (null !== ($song = $this->findOneBy(['uniqueUrl' => $url]))) {
            return $song;
        }
        return null;
    }

    public function getLastXSongs(int $x) {
        # init predis client
        $predis = new PredisCache(new Client());
        # define cache lifetime period as 1 week in seconds
        $cache_lifetime = 604800;

        return $this->createQueryBuilder('s')
                        ->select('s.title, s.uniqueUrl')
                        ->orderBy('s.created', 'desc')
                        ->setMaxResults($x)
                        ->getQuery()
                        ->setResultCacheDriver($predis)
                        ->setResultCacheLifetime($cache_lifetime)
                        ->getArrayResult();
    }

}
