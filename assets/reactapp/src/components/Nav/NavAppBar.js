import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import MenuIcon from '@material-ui/icons/Menu';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { withStyles } from '@material-ui/core/styles';
import { search } from 'src/redux/Songs/action';
import DrawerOpenRight from './DrawerOpenRight';


class NavAppBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchText: '',
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmitSearch = this.handleSubmitSearch.bind(this);
  }

  handleInputChange(event) {
    this.setState({ searchText: event.target.value });
  }

  handleOpen(passedState) {
    let open = !this.state.open;
    if (typeof passedState === 'boolean') {
      open = passedState;
    }
    this.setState({ open });
  }

  handleSubmitSearch(event) {
    event.preventDefault();
    if (this.state.searchText === '') {
      return;
    }
    this.props.search(this.state.searchText);
    this.props.goto('/result');
  }

  render() {
    const { classes } = this.props;
    return (
      <React.Fragment>
        <AppBar title="" className="navAppBar" position="static">
          <Toolbar>
            <IconButton onClick={this.handleOpen} className={classes.menuButton} color="inherit" aria-label="Open drawer">
              <MenuIcon />
            </IconButton>
            <div className={classes.grow} />
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <form onSubmit={this.handleSubmitSearch}>
                <InputBase
                  onChange={this.handleInputChange}
                  value={this.state.searchText}
                  placeholder="ابحث.."
                  classes={{
                      root: classes.inputRoot,
                      input: classes.inputInput,
                    }}
                />
              </form>
            </div>
          </Toolbar>
        </AppBar>
        <DrawerOpenRight open={this.state.open} changeState={this.handleOpen} {...this.props} />
      </React.Fragment>);
  }
}

NavAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
  goto: PropTypes.func.isRequired,
  search: PropTypes.func.isRequired,
};

const styles = theme => ({
  root: {
    width: '100%',
  },
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: -12,
    marginLeft: 20,
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    display: 'flex',
    flexFlow: 'row-reverse',
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '50%',
    '&:focus-within': {
      width: 'auto',
    },
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit,
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing.unit * 9,
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
    width: '100%',
  },
  inputInput: {
    paddingTop: theme.spacing.unit,
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit,
    paddingLeft: theme.spacing.unit * 10,
    transition: theme.transitions.create('width'),
    fontWeight: 'bold',
    width: '100%',
    '&:focus': {
      fontWeight: 'normal',
    },
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
});

export default connect(null, { search })(withStyles(styles)(NavAppBar));

