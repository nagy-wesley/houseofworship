import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import _ from 'lodash';
import { getAllSongs } from 'src/redux/Songs/action';
import { SongsList } from 'src/comp/SongsList';
import { filterSongs } from './Letters';


class SongsIndex extends Component {
  constructor(props) {
    super(props);
    this.state = {
      letter: null,
      filteredSongs: null,
      pageTitle: null,
    };
  }

  componentDidMount() {
    if (_.isEmpty(this.props.songsList)) {
      this.props.getAllSongs();
    }
    this.updateLetter();
  }

  componentDidUpdate(prevProps, prevState) {
    const letter = _.get(this.props, 'match.params.letter', null);
    if (prevState.letter !== letter) {
      this.updateLetter();
    }

    if (this.props.songsList !== prevProps.songsList) {
      this.updateList(letter);
    }
  }

  updateLetter() {
    const letter = _.get(this.props, 'match.params.letter', null);
    this.setState({
      letter,
    }, () => {
      this.updatePageTitle();
      this.updateList(letter);
    });
  }

  updateList(letter) {
    if (!_.isEmpty(this.props.songsList)) {
      const filteredSongs = filterSongs(letter, this.props.songsList);
      this.setState({ filteredSongs });
    }
  }

  updatePageTitle() {
    const pageTitle = this.state.letter
      ? `ترانيم حرف ${this.state.letter}`
      : null;
    this.setState({ pageTitle });
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>{this.state.pageTitle}</title>
          {this.state.letter && <meta name="description" content={`كوردات ترانيم تبدأ بحرف ${this.state.letter}`} />}
        </Helmet>

        <div className="songList">
          {
            this.state.letter ?
              <React.Fragment>
                <h2>{this.state.pageTitle}</h2>
                {this.state.filteredSongs && <SongsList songs={this.state.filteredSongs} />}
              </React.Fragment>
              :
              <React.Fragment>
                {this.props.songsList && <SongsList songs={this.props.songsList} />}
              </React.Fragment>
          }
        </div>
      </React.Fragment>
    );
  }
}

SongsIndex.propTypes = {
  songsList: PropTypes.arrayOf(PropTypes.object).isRequired,
  getAllSongs: PropTypes.func.isRequired,
  match: PropTypes.object, //eslint-disable-line
};

SongsIndex.defaultProps = {
  match: null,
};
const mapStateToProps = state => ({ songsList: state.Songs.songsList });
export default connect(mapStateToProps, { getAllSongs })(SongsIndex);
