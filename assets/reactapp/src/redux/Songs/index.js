import reducer from './reducer';

const initialState = {
  songs: {},
  songsList: [],
};

const songsReducer = (state = initialState, action) => (reducer[action.type]
  ? reducer[action.type](state, action)
  : state);

export default songsReducer;
