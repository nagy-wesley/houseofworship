import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';

export class Done extends Component {
  goto(link) {
    return () => {
      this.context.router.history.push(link);
    };
  }

  render() {
    return (
      <div className="added">
        <div>تم ارسال الرسالة.</div>
        <RaisedButton className="goHome" label="الرجوع الى الرئيسية" onClick={this.goto('/')} />
      </div>);
  }
}

Done.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.shape({ push: PropTypes.func.isRequired, replace: PropTypes.func.isRequired }).isRequired,
    staticContext: PropTypes.object,
  }).isRequired,
};
