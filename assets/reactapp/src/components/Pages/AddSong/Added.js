import React, { Component } from 'react';
import PropTypes from 'prop-types';
import RaisedButton from 'material-ui/RaisedButton';

export class Added extends Component {
  goto(link) {
    return () => {
      this.context.router.history.push(link);
    };
  }

  render() {
    return (
      <div className="added">
        <div>تم إضافة الترنيمة بنجاح</div>
        <RaisedButton className="addNew"label="اضف ترنيمة اخرى" primary onClick={this.props.resetState} />
        <RaisedButton className="goHome" label="الرجوع الى الرئيسية" onClick={this.goto('/')} />
      </div>);
  }
}

Added.propTypes = {
  resetState: PropTypes.func.isRequired,
};
Added.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.shape({ push: PropTypes.func.isRequired, replace: PropTypes.func.isRequired }).isRequired,
    staticContext: PropTypes.object,
  }).isRequired,
};
