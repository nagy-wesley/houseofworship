<?php

namespace App\Controller;

use App\Repository\FeaturedSongRepository;
use App\Repository\SongRepository;
use App\Service\SearchService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use App\Entity\Song;

class DefaultController extends Controller
{

    public function index()
    {

        return $this->render('index.html.twig');
    }

    /**
     * @Route("/api/getAll")
     */
    public function getAllAction()
    {
        $songsRepo = $this->get('App\Repository\SongRepository');
        $songs = $songsRepo->getAllSongs();
        return new JsonResponse($songs);
    }

    /**
     * @Route("/api/getSong/{url}")
     */
    public function getSongAction(string $url)
    {
        $songsRepo = $this->get('App\Repository\SongRepository');
        $song = $songsRepo->getSongByURL($url);
        if ($song) {
            return new JsonResponse([
                $song->getId() =>
                    ['id' => $song->getId(),
                        'title' => $song->getTitle(),
                        'content' => $song->getContent(),
                        'uniqueUrl' => $song->getUniqueUrl()]
            ]);
        }

        return new JsonResponse(["message" => "Song not found, please check url."], 404);
    }

    /**
     * @Route("/api/save-song")
     */
    public function saveSongAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        try {
            if (getenv('SAFE_MODE')) {

                $mailer = $this->get('mailer_service');
                $title = "Suspicious Activity";
                $mailer->contact($data, $title);

                return new JsonResponse(["message" => "can't add song now, please try again.", "isValid" => false], 400);
            }

            $validator = $this->get('validator');
            $song = new Song();
            $song->setTitle($data['title']);
            $song->setContent($data['content']);
            $song->setUniqueUrl(str_replace(' ', '-', strtolower($data['title'])));
            $song->setCreated(new \DateTime('now'));
            $errors = $validator->validate($song);
            if (count($errors) > 0) {
                return new JsonResponse(["message" => "Song data is incomplete, please try again.", "isValid" => $errors->get(0)->getMessage()], 400);
            }
            /** @var App\Repository\SongRepository */
            $songsRepo = $this->get('App\Repository\SongRepository');

            /** @var Song */
            $newSong = $songsRepo->saveSong($song);

            //notifing admins
            /** @var App\Service\MailService */
            $mailer = $this->get('mailer_service');
            $mailer->newSongAlert($newSong);

            return new JsonResponse(["message" => "song added", "id" => $newSong->getId()], 200);
        } catch (\Throwable $e) {
            return new JsonResponse(["message" => $e->getMessage()], 500);
        }
    }

    /**
     * @Route("/api/request-song")
     *
     */
    public function requestSong(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data['title'])) {
            return new JsonResponse(["message" => 'no title found'], 400);
        }
        /** @var App\Service\MailService */
        $mailer = $this->get('mailer_service');
        $title = "new song request";
        $mailer->contact($data, $title);

        return new JsonResponse(["message" => 'request sent'], 200);
    }

    /**
     * @Route("/api/contact")
     *
     */
    public function contact(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (empty($data['email'])) {
            return new JsonResponse(["message" => 'no email found'], 400);
        }
        if (empty($data['message'])) {
            return new JsonResponse(["message" => 'no message found'], 400);
        }
        /** @var App\Service\MailService */
        $mailer = $this->get('mailer_service');
        $title = "contact";
        $mailer->contact($data, $title);

        return new JsonResponse(["message" => 'message sent'], 200);
    }

    /** @Route("/api/homepageSongs/{type}") */
    public function getLatestAction(string $type)
    {
        $limit = 10;
        if ($type == 'latest') {

            /** @var SongRepository $songsRepo */
            $songsRepo = $this->get('App\Repository\SongRepository');
            $songs = $songsRepo->getLastXSongs($limit);
        } else if ($type == 'featured') {

            /** @var FeaturedSongRepository $featuredRepo */
            $featuredRepo = $this->get('App\Repository\FeaturedSongRepository');
            $songs = $featuredRepo->getLatestFeaturedSongs($limit);
        }

        return new JsonResponse($songs);
    }


    /**
     * @Route("/api/search/{keyword}")
     * @param string $keyword
     * @return JsonResponse
     */
    public function searchSongs(string $keyword)
    {
        /** @var SearchService $serachService */
        $serachService = $this->get('search_service');
        $results = $serachService->findSongElastica($keyword);
        return new JsonResponse($results);
    }

}
