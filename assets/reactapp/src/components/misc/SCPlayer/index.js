import React from 'react';

const SCPlayer = () => (
  /* eslint-disable */
  <div className="SCPlayer">
    <iframe title="soundCloudPlayer" width="100%" height="450" scrolling="no" frameBorder="yes" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/530844840&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true" />
  </div>
  /* eslint-enable */
);

export default SCPlayer;
