const path = require('path');
module.exports = {
    "extends": [
        "airbnb",
        "plugin:promise/recommended"
    ],
    "parser": "babel-eslint",
    "plugins": [
        "react",
        "import",
        "promise"
    ],
    "rules": {
        "linebreak-style": "off",
        "import/prefer-default-export": "off",
        "react/jsx-filename-extension": [1, {"extensions": [".js", ".jsx"]}],
        "import/no-extraneous-dependencies": ["error", {"devDependencies": true}],
        "import/no-named-as-default": "off",
        "no-unused-expressions": ["error", { "allowTernary": true }],
        "max-len": [1, 140, 2, { "ignoreComments": true }],
        "jsx-a11y/anchor-is-valid": false,
        'react/forbid-prop-types': 0,
    },
    "settings": {
        "import/resolver": {
            "alias": {
                "map": [
                    ["src/comp", path.resolve(__dirname, "./assets/reactapp/src/components",)],
                    ["src/redux", path.resolve(__dirname, "./assets/reactapp/src/redux",)],
                    ["assets", path.resolve(__dirname, "./assets/reactapp/assets",)],
                ]
            },
            "webpack": {
                "config": "webpack.config.js"
            }
        }
    },
    "globals": {
        "window": true,
        "fetch": true,
    }
}