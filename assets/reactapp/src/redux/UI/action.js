export const setAppSize = width => (dispatch) => {
  if (width < 600) {
    dispatch({ type: 'SET_ISMOBILE' });
  } else if (width < 999) {
    dispatch({ type: 'SET_ISTAB' });
  } else {
    dispatch({ type: 'SET_ISDESKTOP' });
  }
};
