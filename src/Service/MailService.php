<?php

namespace App\Service;

use App\Entity\Song;
use Swift_Message;
use Swift_Mailer;

class MailService {

    private $_mailer;

    public function __construct(Swift_Mailer $mailer) {
        $this->_mailer = $mailer;
    }

    public function newSongAlert(Song $song) {
        $title = 'تم إضافة ترنيمة جديدة';
        $url = 'https://www.house-worship.com/song/' . $song->getUniqueUrl();
        
        if (getenv('APP_ENV') == 'dev') {
            $to = 'nagy.wesley@gmail.com';
            $url = 'http://www.house-worshipdev.com/song/' . $song->getUniqueUrl();
        }
        $body = 'a new song: ' . $song->getTitle() . ' was added <br/>'
                . 'please check it at <a href="' . $url . '">HERE </a>';
        $this->sendEmail($body, $title);
    }


    public function contact($data, $title){
        $body = implode('-', $data);
        $this->sendEmail($body, $title);
    }

    private function sendEmail(string $body, string $title) {
        $to = ['jean.george.aziz@gmail.com', 'nagy.wesley@gmail.com'];
        if (getenv('APP_ENV') == 'dev') {
            $to = 'nagy.wesley@gmail.com';
        }

        try {
            $message = (new Swift_Message($title))
                    ->setFrom('new@house-worship.com')
                    ->setTo($to)
                    ->setBody($body, 'text/html');
            $this->_mailer->send($message);
            return true;
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die;
        }
    }

}
