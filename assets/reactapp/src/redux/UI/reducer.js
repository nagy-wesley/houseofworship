function setIsDesktop(state) {
  return {
    ...state,
    isDesktop: true,
    isMobile: false,
    isTab: false,
  };
}

function setIsMobile(state) {
  return {
    ...state,
    isDesktop: false,
    isMobile: true,
    isTab: false,
  };
}

function setIsTab(state) {
  return {
    ...state,
    isDesktop: false,
    isMobile: false,
    isTab: true,
  };
}

function showLoading(state) {
  return {
    ...state,
    isLoading: true,
  };
}

function hideLoading(state) {
  return {
    ...state,
    isLoading: false,
  };
}


export default {
  SET_ISDESKTOP: setIsDesktop,
  SET_ISMOBILE: setIsMobile,
  SET_ISTAB: setIsTab,
  SEARCH_STARTED: showLoading,
  SEARCH_COMPLETED_SUCCESS: hideLoading,
};
