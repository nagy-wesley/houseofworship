const Encore = require('@symfony/webpack-encore');
const path = require('path');

Encore
// the project directory where compiled assets will be stored
  .setOutputPath('public/build/')
// the public path used by the web server to access the previous directory
  .setPublicPath('/build')
  .cleanupOutputBeforeBuild()
  .enableSourceMaps(!Encore.isProduction())
  .enableReactPreset()
// uncomment to create hashed filenames (e.g. app.abc123.css)
//    .enableVersioning(Encore.isProduction())

// uncomment to define the assets of the project
  .addEntry('js/app', './assets/reactapp/src/app.js')
  .addStyleEntry('css/app', './assets/reactapp/stylesheets/app.scss')

// show OS notifications when builds finish/fail
  .enableBuildNotifications()

// uncomment if you use Sass/SCSS files
  .enableSassLoader()
  .addLoader({
    test: /\.(js|jsx)$/,
    loader: 'eslint-loader',
    exclude: [/node_modules/],
    enforce: 'pre',
    options: {
      configFile: './.eslintrc.js',
      emitWarning: true,
    },
  })

// uncomment for legacy applications that require $/jQuery as a global variable
// .autoProvidejQuery()

  .configureBabel((babelConfig) => {
    // add additional presets
    babelConfig.presets.push('env');
    babelConfig.presets.push('react');
    // no plugins are added by default, but you can add some
    babelConfig.plugins.push('transform-object-rest-spread');
  });
const config = Encore.getWebpackConfig();
config.resolve.alias =
    {
      'src/comp': path.resolve(__dirname, './assets/reactapp/src/components'),
      'src/redux': path.resolve(__dirname, './assets/reactapp/src/redux'),
      assets: path.resolve(__dirname, './assets/reactapp/assets'),
    };


module.exports = config;
