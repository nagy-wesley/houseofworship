<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FeaturedSongRepository")
 */
class FeaturedSong
{

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Song")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Song;

    /**
     * @ORM\Column(type="datetime")
     */
    private $FeaturedDate;

    public function getSong(): ?Song
    {
        return $this->Song;
    }

    public function setSong(?Song $Song): self
    {
        $this->Song = $Song;

        return $this;
    }

    public function getFeaturedDate(): ?\DateTimeInterface
    {
        return $this->FeaturedDate;
    }

    public function setFeaturedDate(\DateTimeInterface $FeaturedDate): self
    {
        $this->FeaturedDate = $FeaturedDate;

        return $this;
    }
}
