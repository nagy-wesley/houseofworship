import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import InputBase from '@material-ui/core/InputBase';
import { withStyles } from '@material-ui/core/styles';
import purple from '@material-ui/core/colors/purple';
import SearchIcon from '@material-ui/icons/Search';
import InputAdornment from '@material-ui/core/InputAdornment';
import { search } from 'src/redux/Songs/action';
import _ from 'lodash';


class HeadSearch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      searchText: '',
    };
    this.handleOpen = this.handleOpen.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmitSearch = this.handleSubmitSearch.bind(this);
  }

  handleInputChange(event) {
    this.setState({ searchText: event.target.value });
  }

  handleOpen(passedState) {
    let open = !this.state.open;
    if (typeof passedState === 'boolean') {
      open = passedState;
    }
    this.setState({ open });
  }

  handleSubmitSearch(event) {
    event.preventDefault();
    if (this.state.searchText === '') {
      return;
    }
    this.props.search(this.state.searchText);
    this.context.router.history.push('/result');
  }

  render() {
    if (this.props.isMobile) {
      return null;
    }

    const { classes } = this.props;
    return (
      <div className={classes.headSearch}>
        <form onSubmit={this.handleSubmitSearch}>
          <InputBase
            id="headSearch-input"
            placeholder="ابحث عن ترنيمة"
            classes={{
                        root: classes.bootstrapRoot,
                        input: classes.bootstrapInput,
                    }}
            onChange={this.handleInputChange}
            endAdornment={
              <InputAdornment
                classes={{
                                root: classes.adornment,
                            }}
                position="start"
              >
                <SearchIcon />
              </InputAdornment>
                    }
          />
        </form>
      </div>
    );
  }
}


const styles = theme => ({
  headSearch: {
    gridColumn: '2/3',
    gridRow: '3/4',
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  margin: {
    margin: theme.spacing.unit,
  },
  cssLabel: {
    '&$cssFocused': {
      color: purple[500],
    },
  },
  cssFocused: {},
  cssUnderline: {
    '&:after': {
      borderBottomColor: purple[500],
    },
  },
  cssOutlinedInput: {
    '&$cssFocused $notchedOutline': {
      borderColor: purple[500],
    },
  },
  notchedOutline: {},
  bootstrapRoot: {
    width: '100%',
    'label + &': {
      marginTop: theme.spacing.unit * 3,
    },
  },
  adornment: {
    marginRight: '-7%',
  },
  bootstrapInput: {
    borderRadius: 4,
    backgroundColor: theme.palette.common.white,
    border: '1px solid #ced4da',
    fontSize: 16,
    padding: '10px 12px',
    transition: theme.transitions.create(['border-color', 'box-shadow']),
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    '&:focus': {
      borderColor: '#80bdff',
      boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
    },
  },
  bootstrapFormLabel: {
    fontSize: 18,
  },
});


HeadSearch.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.shape({ push: PropTypes.func.isRequired, replace: PropTypes.func.isRequired }).isRequired,
    staticContext: PropTypes.object,
  }).isRequired,
};


HeadSearch.propTypes = {
  classes: PropTypes.object.isRequired,
  search: PropTypes.func.isRequired,
  isMobile: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    isMobile: _.get(state, 'UI.isMobile', false),
  };
}


export default connect(mapStateToProps, { search })(withStyles(styles)(HeadSearch));
