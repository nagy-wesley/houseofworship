import React from 'react';
import Paper from 'material-ui/Paper';
import PropTypes from 'prop-types';

const style = {
  minHeight: 300,
  minWidth: 450,
  margin: 20,
  padding: 25,
  textAlign: 'right',
  display: 'inline-block',
  borderRadius: 16,
};


const SongText = ({ lyrics }) => (
  /* eslint-disable */
  <Paper style={style} zDepth={4}>
    <div className="songContent" dangerouslySetInnerHTML={{ __html: lyrics }} />
  </Paper>
  /* eslint-enable */
);

SongText.propTypes = {
  lyrics: PropTypes.string.isRequired,
};

export default SongText;
