<?php
/**
 * Created by PhpStorm.
 * User: nagy
 * Date: 11/15/18
 * Time: 8:51 PM
 */

namespace App\Service;


use App\Entity\Song;
use FOS\ElasticaBundle\Finder\TransformedFinder;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SearchService implements ContainerAwareInterface
{

    private CONST ANALYZER_NAME = 'how_analyzer';

    private CONST TITLE_FIELD = 'title';

    private CONST CONTENT_FIELD = 'content';

    /**
     * @var Container
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;

    }

    /**
     * @param string $keyword
     * @return array
     * @throws \Exception
     */
    public function findSongElastica(string $keyword)
    {
        /** @var TransformedFinder $finder */
        $finder = $this->container->get('fos_elastica.finder.how.song');
        $searchKeyWord = '*' . str_replace(' ', '*', $keyword) . '*';

        $contentWildCard = new \Elastica\Query\Wildcard(self::CONTENT_FIELD, $searchKeyWord);


        $contentString = new \Elastica\Query\QueryString($keyword);
        $contentString->setAnalyzer(self::ANALYZER_NAME);

        $boolQuery = new \Elastica\Query\BoolQuery();
        $boolQuery->addShould($contentWildCard);
        $boolQuery->addShould($contentString);


        $titleWildCard = new \Elastica\Query\Wildcard(self::TITLE_FIELD, $searchKeyWord);
        $titleString = new \Elastica\Query\QueryString($keyword);
        $titleString->setAnalyzer(self::ANALYZER_NAME);
        $titleString->setBoost(6);

        $boolQuery->addShould($titleWildCard);
        $boolQuery->addShould($titleString);


        $results = $finder->findHybrid($boolQuery);

        $songs = [];
        $highestScore = 0;
        foreach ($results as $result) {
            /** @var Song $song */
            $song = $result->getTransformed();
            $res = $result->getResult();
            if ($res->getScore() > $highestScore) {
                $highestScore = $res->getScore();
            }

            if ($res->getScore() >= ($highestScore / 2)) {
                $songs[] =
                    [
                        'id' => $song->getId(),
                        'title' => $song->getTitle(),
                        'uniqueUrl' => $song->getUniqueUrl(),
                        'score' => $res->getScore()
                    ];
            }
        }
        return $songs;

    }
}