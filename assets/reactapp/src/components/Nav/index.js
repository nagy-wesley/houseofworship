import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Paper from 'material-ui/Paper';
import _ from 'lodash';
import NavMenu from './NavMenu';
import NavAppBar from './NavAppBar';

class Nav extends Component {
  constructor(props) {
    super(props);
    this.goto = this.goto.bind(this);
  }
  goto(link) {
    this.context.router.history.push(link);
  }

  render() {
    if (this.props.isMobile) {
      return (
        <NavAppBar goto={this.goto} />
      );
    }
    return (
      <Paper className="navigation">
        <NavMenu goto={this.goto} />
      </Paper>);
  }
}

Nav.contextTypes = {
  router: PropTypes.shape({
    history: PropTypes.shape({ push: PropTypes.func.isRequired, replace: PropTypes.func.isRequired }).isRequired,
    staticContext: PropTypes.object,
  }).isRequired,
};

Nav.propTypes = {
  isMobile: PropTypes.bool.isRequired,
};

function mapStateToProps(state) {
  return {
    isMobile: _.get(state, 'UI.isMobile', false),
  };
}

export default connect(mapStateToProps)(Nav);
