import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { lettersArray } from 'src/comp/Pages/SongsIndex/Letters';
import { withStyles } from '@material-ui/core/styles';

class NavMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      anchorEl: null,
    };

    this.handleClickListItem = this.handleClickListItem.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.goToMenuLink = this.goToMenuLink.bind(this);
  }


  handleClickListItem(event) {
    this.setState({ anchorEl: event.currentTarget });
  }

  handleClose() {
    this.setState({ anchorEl: null });
  }

  goToMenuLink(link) {
    return () => {
      this.props.goto(link);
      this.setState({ anchorEl: null });
    };
  }

  render() {
    const { anchorEl } = this.state;
    const { classes } = this.props;

    return (
      <div>
        <List component="nav">
          <ListItem
            classes={{
              root: classes.menuListItem,
            }}
            button
            aria-label="الرئيسية"
            onClick={this.goToMenuLink('/')}
          >
            <ListItemText
              classes={{
                root: classes.navTextBase,
              }}
              primary="الرئيسية"
            />
          </ListItem>

          <ListItem
            classes={{
              root: classes.menuListItem,
            }}
            button
            aria-haspopup="true"
            aria-label="فهرس الترانيم"
            onClick={this.handleClickListItem}
          >
            <ListItemText
              classes={{
                root: classes.navTextBase,
              }}
              primary="فهرس الترانيم"
            />
          </ListItem>

          <ListItem
            classes={{
              root: classes.menuListItem,
            }}
            button
            aria-label="اطلب ترنيمة"
            onClick={this.goToMenuLink('/request')}
          >
            <ListItemText
              classes={{
                root: classes.navTextBase,
              }}
              primary="اطلب ترنيمة"
            />
          </ListItem>

          <ListItem
            classes={{
              root: classes.menuListItem,
            }}
            button
            aria-label="شارك بترنيمة"
            onClick={this.goToMenuLink('/add-song')}
          >
            <ListItemText
              classes={{
                root: classes.navTextBase,
              }}
              primary="شارك بترنيمة"
            />
          </ListItem>

          <ListItem
            classes={{
              root: classes.menuListItem,
            }}
            button
            aria-label="اتصل بنا"
            onClick={this.goToMenuLink('/contact-us')}
          >
            <ListItemText
              classes={{
                root: classes.navTextBase,
              }}
              primary="اتصل بنا"
            />

          </ListItem>
        </List>


        {/* menu fehrs el taraneem */}
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          {_.map(lettersArray, letter => (
            <MenuItem
              key={letter}
              onClick={this.goToMenuLink(`/songsIndex/${letter}`)}
            >
              {`ترانيم حرف ${letter}`}
            </MenuItem>
                    ))}
        </Menu>
      </div>
    );
  }
}


NavMenu.propTypes = {
  goto: PropTypes.func,
  classes: PropTypes.object.isRequired,
};

NavMenu.defaultProps = {
  goto: () => {
  },
};


const styles = {
  navTextBase: {
    textAlign: 'right',
  },
  menuListItem: {
    paddingRight: 0,
  },
};

export default withStyles(styles)(NavMenu);
