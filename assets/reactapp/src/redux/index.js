import { createStore, applyMiddleware } from 'redux';
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant';
import { composeWithDevTools } from 'redux-devtools-extension';
import { googleAnalytics } from 'src/redux/middleware/reactGAMiddlewares';
import { routerMiddleware } from 'react-router-redux';
import thunk from 'redux-thunk';
import rootReducer from 'src/redux/rootReducer';


export default (history) => {
  const middleware = routerMiddleware(history);

  const store = createStore(
    rootReducer, {},
    composeWithDevTools(applyMiddleware(
      thunk,
      middleware,
      googleAnalytics,
      reduxImmutableStateInvariant(),
    )),
  );

  return store;
};
