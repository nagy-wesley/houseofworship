import { combineReducers } from 'redux';
import Songs from 'src/redux/Songs';
import UI from 'src/redux/UI';
import { routerReducer } from 'react-router-redux';

export default combineReducers({ Songs, UI, router: routerReducer });
