import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { SongsList } from 'src/comp/SongsList';
import NoResults from './NoResults';

const SearchResult = (props) => {
  if (props.isLoading) {
    return <React.Fragment>جاري البحث</React.Fragment>;
  }

  if (props.searchResult.length === 0) {
    return <NoResults />;
  }

  return (
    <div className="homepageContent">
      <h2>نتيجة البحث</h2>
      {props.searchResult.length > 0 && <SongsList songs={props.searchResult} />}
    </div>
  );
};


SearchResult.propTypes = {
  searchResult: PropTypes.arrayOf(PropTypes.object),
  isLoading: PropTypes.bool.isRequired,
};

SearchResult.defaultProps = {
  searchResult: [],
};

const mapStateToProps = state => ({ searchResult: state.Songs.searchResult, isLoading: state.UI.isLoading });
export default connect(mapStateToProps)(SearchResult);
