import React from 'react';
import PropTypes from 'prop-types';
import Drawer from '@material-ui/core/Drawer';
import NavMenu from './NavMenu';

export const DrawerOpenRight = (props) => {
  const gotoAndClose = (link) => {
    props.changeState(false);
    props.goto(link);
  };

  return (
    <div>
      <Drawer
        width={150}
        open={props.open}
        anchor="right"
        onClose={() => { props.changeState(false); }}
      >
        <NavMenu goto={gotoAndClose} />
      </Drawer>
    </div>);
};

DrawerOpenRight.propTypes = {
  changeState: PropTypes.func.isRequired,
  goto: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};


export default DrawerOpenRight;
