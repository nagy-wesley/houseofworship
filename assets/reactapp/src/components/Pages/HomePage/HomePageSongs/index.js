import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getHomePageSongs } from 'src/redux/Songs/action';
import { SongsList } from 'src/comp/SongsList';
import _ from 'lodash';

class HomePageSongs extends Component {
  componentDidMount() {
    if (_.isEmpty(this.props.songs)) {
      this.props.getHomePageSongs(this.props.type);
    }
  }
  render() {
    return (
      <div className={this.props.type}>
        <h2>
          {
          this.props.type === 'latest'
            ? 'ترانيم جديدة'
            : 'ترانيم مميزة'
        }
        </h2>
        <SongsList songs={this.props.songs} />
      </div>);
  }
}

HomePageSongs.propTypes = {
  songs: PropTypes.arrayOf(PropTypes.object).isRequired,
  getHomePageSongs: PropTypes.func.isRequired,
  type: PropTypes.string.isRequired,
};

const mapStateToProps = (state, ownProps) => {
  let songs = [];
  switch (ownProps.type) {
    case 'latest':
      songs = _.get(state, 'Songs.latest', []);
      break;
    default:
      songs = _.get(state, 'Songs.featured', []);
  }
  return { songs };
};

export default connect(mapStateToProps, { getHomePageSongs })(HomePageSongs);
