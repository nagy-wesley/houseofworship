import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { Recap } from 'src/comp/misc/Scripts/Recaptcha';
import { Added } from 'src/comp/Pages/AddSong/Added';
import { saveSong } from 'src/redux/Songs/action';
import _ from 'lodash';

class AddSong extends Component {
  constructor(props) {
    super(props);
    this.verifyCallback = this.verifyCallback.bind(this);
    this.expiredCallback = this.expiredCallback.bind(this);
    this.resetState = this.resetState.bind(this);
  }

  componentWillMount() {
    this.resetState();
  }

  verifyCallback() {
    this.setState({ disabledAdd: false, recaptchaValidated: true });
  }

  expiredCallback() {
    this.setState({ recaptchaValidated: false });
  }


  resetState() {
    this.setState({
      recaptchaValidated: false,
      disabledAdd: true,
      added: false,
      song: {
        title: '',
        content: '',
      },
      titleError: '',
      contentError: '',
    });
  }

  saveSong() {
    if (!this.state.recaptchaValidated) {
      this.setState({ disabledAdd: true });
      return;
    }

    const emptyTitle = _.isEmpty(this.state.song.title);

    (emptyTitle)
      ? this.setState({ titleError: 'ادخل اسم ترنيمة' })
      : this.setState({ titleError: '' });

    const emptyContent = _.isEmpty(this.state.song.content);
    (emptyContent)
      ? this.setState({ contentError: 'ادخل كوردات الترنيمة' })
      : this.setState({ contentError: '' });

    if (!emptyTitle && !emptyContent) {
      this.props.saveSong(this.state.song);
      this.setState({ added: true });
    }
  }

  handleInput(e, value) {
    const tempSong = {
      ...this.state.song,
      [e.target.name]: value,
    };
    this.setState({ song: tempSong });
  }

  handleTextareaInput(e) {
    let formatedContent = e.target.value.replace(/\n\r?/g, '<br/>');
    formatedContent = formatedContent.replace(/(\s)(?![^<]*>|[^<>]*<\/)/g, '&nbsp;');
    const tempSong = {
      ...this.state.song,
      content: formatedContent,
    };
    this.setState({ song: tempSong });
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>شارك بترنيمة</title>
          <meta name="description" content="عارف كوردات ترنيمة؟ شارك بيها مع الجسد" />
        </Helmet>
        <div className="form">
          {
                    !this.state.added
                        ? (
                          <React.Fragment>
                            <div className="form-message">
                              <h3>شارك بكوردات و كلمات ترنيمة</h3>
                              <p>بيت التسبيح بيؤمن جدا بالملكية الفكرية، الرجاء عدم الإقتباس من كتب أو مواقع أخرى، ما
                                    تحفظه كافي جدا للمشاركة.
                              </p>
                            </div>
                            <Paper className="form-paper addsong">
                              <TextField
                                className="right-col"
                                name="title"
                                hintText="اسم الترنيمة*"
                                errorText={this.state.titleError}
                                onChange={(e, v) => {
                                        this.handleInput(e, v);
                                    }}
                              />
                              <br />
                              <TextField
                                className="fullwidth-col songContent"
                                name="content"
                                hintText="اضف الترنيمة هنا*"
                                fullWidth
                                multiLine
                                errorText={this.state.contentError}
                                onChange={(e) => {
                                        this.handleTextareaInput(e);
                                    }}
                              />
                              <br />
                            </Paper>
                            <div className="save-button">
                              <RaisedButton
                                label="اضف التـــرنيمة"
                                primary={!this.state.disabledAdd}
                                disableTouchRipple={this.state.disabledAdd}
                                onClick={() => {
                                        this.saveSong();
                                    }}
                              />

                            </div>
                            <Recap verifyCallback={this.verifyCallback} expiredCallback={this.expiredCallback} />

                          </React.Fragment>)
                        : <Added resetState={this.resetState} />

                }
        </div>
      </React.Fragment>);
  }
}

AddSong.propTypes = {
  saveSong: PropTypes.func.isRequired,
};

export default connect(null, { saveSong })(AddSong);
