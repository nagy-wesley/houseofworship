<?php

namespace App\Repository;

use App\Entity\FeaturedSong;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\Common\Cache\PredisCache;
use Predis\Client;

/**
 * @method FeaturedSong|null find($id, $lockMode = null, $lockVersion = null)
 * @method FeaturedSong|null findOneBy(array $criteria, array $orderBy = null)
 * @method FeaturedSong[]    findAll()
 * @method FeaturedSong[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FeaturedSongRepository extends ServiceEntityRepository {

    public function __construct(RegistryInterface $registry) {
        parent::__construct($registry, FeaturedSong::class);
    }

    /**
     * @return FeaturedSong[] Returns an array of FeaturedSong objects
     */
    public function getLatestFeaturedSongs($limit) {
        # init predis client
        $predis = new PredisCache(new Client());
        # define cache lifetime period as 1 week in seconds
        $cache_lifetime = 604800;

        return $this->createQueryBuilder('f')
                        ->select('s.title, s.uniqueUrl')
                        ->innerJoin('App\Entity\Song', 's', 'WITH', 's= f.Song')
                        ->orderBy('f.FeaturedDate', 'DESC')
                        ->setMaxResults($limit)
                        ->getQuery()
                        ->setResultCacheDriver($predis)
                        ->setResultCacheLifetime($cache_lifetime)
                        ->getResult();
    }

    /*
      public function findOneBySomeField($value): ?FeaturedSong
      {
      return $this->createQueryBuilder('f')
      ->andWhere('f.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      }
     */
}
