sudo apt-get update && sudo apt-get install nginx curl php-fpm php-mysql php-xml php-zip php-curl zip unzip nodejs curl gnupg2 git redis-server redis-tools mysql-server

######
sudo mysql_secure_installation
###
sudo mysql -u root
###
DROP USER 'root'@'localhost';
###
CREATE USER 'root'@'%' IDENTIFIED BY '3ezbtNASHED';
###
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
######

CREATE USER 'how'@'localhost' IDENTIFIED BY 'passW0rd!';
GRANT ALL PRIVILEGES ON how. * TO 'how'@'localhost';
FLUSH PRIVILEGES;

sudo vim /etc/php/7.2/fpm/php.ini
change to : 
cgi.fix_pathinfo=0
extension=curl.so   

sudo systemctl restart php7.2-fpm






git clone https://nagy-wesley@bitbucket.org/nagy-wesley/houseofworship.git

sudo php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" 
sudo php -r "if (hash_file('SHA384', 'composer-setup.php') === '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" 
sudo php composer-setup.php 
sudo php -r "unlink('composer-setup.php');" 
sudo mv composer.phar /usr/local/bin/composer

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn

##install elasticsearch
- docker pull docker.elastic.co/elasticsearch/elasticsearch:6.5.4
- docker run -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.5.4
##

sudo composer install

##
#update .env in this step
##
sudo php bin/console doctrine:database:create
sudo php bin/console doctrine:migrations:migrate




#################
#/etc/nginx/nginx.conf

	##
	# Gzip Settings
	##
        gzip on;
        gzip_vary on;
        gzip_min_length 256;
        gzip_comp_level    5;
        gzip_proxied       any;
        gzip_disable "MSIE [1-6]\.";
        gzip_types application/atom+xml application/javascript application/json application/ld+json application/manifest+json application/rss+xml application/vnd.geo+json application/vnd.ms-fontobject application/x-font-ttf application/x-web-app-manifest+json application/xhtml+xml application/xml font/opentype image/bmp image/svg+xml image/x-icon text/cache-manifest text/css text/plain text/vcard text/vnd.rim.location.xloc text/vtt text/x-component text/x-cross-domain-policy;

###################################


# /etc/nginx/sites-available/default 

server {
    server_name house-worship.com www.house-worship.com;
    root /var/www/houseofworship/public;

    location / {
        # try to serve file directly, fallback to index.php
        try_files $uri /index.php$is_args$args;
    }

    location ~ ^/index\.php(/|$) {


	proxy_set_header X-Prerender-Token mICYeGdYZ58fv7npXcYZ;
        set $prerender 0;
        if ($http_user_agent ~* "googlebot|baiduspider|twitterbot|facebookexternalhit|rogerbot|linkedinbot|embedly|quora link preview|showyoubot|outbrain|pinterest|slackbot|vkShare|W3C_Validator") {
        set $prerender 1;
    }
        if ($args ~ "_escaped_fragment_") {
            set $prerender 1;
        }
        if ($http_user_agent ~ "Prerender") {
            set $prerender 0;
        }
          if ($uri ~ "\.(js|css|xml|less|png|jpg|jpeg|gif|pdf|doc|txt|ico|rss|zip|mp3|rar|exe|wmv|doc|avi|ppt|mpg|mpeg|tif|wav|mov|psd|ai|xls|mp4|m4a|swf|dat|dmg|iso|flv|m4v|torrent|ttf|woff)") {
        set $prerender 0;
    } 
        
        #resolve using Google's DNS server to force DNS resolution and prevent caching of IPs
        resolver 8.8.8.8;
 
        if ($prerender = 1) {
            
            #setting prerender as a variable forces DNS resolution since nginx caches IPs and doesnt play well with load balancing
            set $prerender "service.prerender.io";
            rewrite .* /$scheme://$host$request_uri? break;
            proxy_pass http://$prerender;
        }
   

        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;

        # optionally set the value of the environment variables used in the application
        # fastcgi_param APP_ENV production;
        # fastcgi_param APP_SECRET <app-secret-id>;
        # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

        # When you are using symlinks to link the document root to the
        # current version of your application, you should pass the real
        # application path 
        # FPM.
        # Otherwise, PHP's OPcache may not properly detect changes to
        # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
        # for more information).
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        # Prevents URIs that include the front controller. This will 404:
        # http://domain.tld/index.php/some-path
        # Remove the internal directive to allow URIs like this
        internal;
    }

    # return 404 for all other php files not matching the front controller
    # this prevents access to other php files you don't want to be accessible.
    location ~ \.php$ {
        return 404;
    }
   
listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/house-worship.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/house-worship.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot


}

server {
    if ($host = www.house-worship.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    if ($host = house-worship.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    server_name house-worship.com www.house-worship.com;
    listen 80;
    return 404; # managed by Certbot




}



####################################

sudo nginx -t

    sudo systemctl restart php7.2-fpm nginx  nginx.service

#prod
sudo bin/console cache:clear && sudo yarn install && sudo yarn run encore production
#dev
sudo bin/console cache:clear && sudo yarn install && sudo yarn run encore dev


###################

#  install ssl
##  https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx

####################

#####################