import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import _ from 'lodash';

export const SongsList = props => (
  <div className="songList">
    {
            _.map(props.songs, song => (
              <React.Fragment key={song.uniqueUrl}>
                {song.title && (<Link to={`/song/${song.uniqueUrl}`}>{song.title}</Link>)}
              </React.Fragment>))
        }
  </div>

);

SongsList.propTypes = {
  songs: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default SongsList;
