/* eslint-disable */
export const getAllSongs = (dispatch) => {
  return dispatch => fetch(`/api/getAll`).then(res => res.json()).then(songsList => dispatch({type: 'LOAD_ALL_SONGS_SUCCESS', songsList}), err => dispatch({type: 'FETCH_FAIL', err}));

}

export const getSong = (songUrl) => {
  return dispatch => fetch(`/api/getSong/${songUrl}`).then(res => res.json()).then(song => dispatch({type: 'LOAD_SINGLE_SONG_SUCCESS', song}), err => dispatch({type: 'FETCH_FAIL', err}));
}

export const search = (searchText) => {
  return dispatch => {
    dispatch({type: 'SEARCH_STARTED'});
    fetch(`/api/search/${searchText}`).then(res => res.json()).then(searchResult => dispatch({type: 'SEARCH_COMPLETED_SUCCESS', searchResult}), err => dispatch({type: 'FETCH_FAIL', err}));}
}

export const saveSong = (songData) => {
  return dispatch => {
    dispatch({type: 'SAVING_SONG_STARTED'});
    fetch('/api/save-song', {
      method: 'POST',
      body: JSON.stringify(songData)
    }).then(res => res.json()).then(result => dispatch({type: 'SAVING_SONG_SUCCESS', result}), err => dispatch({type: 'SAVING_SONG_FAIL', err}))
  }
}

export const requestSong = (form) => {
  return dispatch => {
    dispatch({type: 'REQUEST_SONG_STARTED'});
    fetch('/api/request-song', {
      method: 'POST',
      body: JSON.stringify(form)
    }).then(res => res.json()).then(result => dispatch({type: 'REQUEST_SONG_SUCCESS', result}), err => dispatch({type: 'REQUEST_SONG_FAIL', err}))
  }
}

export const getHomePageSongs = (listType) => {
  return dispatch => {
    fetch(`/api/homepageSongs/${listType}`).then(res => res.json()).then(songs => dispatch({type: 'LOAD_HOMEPAGE_SONGS_SUCCESS', songs, listType}), err => dispatch({type: 'FETCH_FAIL', err}));
  }
}

export const sendContactUsMessage = (form) => {
  return dispatch => {
    dispatch({type: 'CONTACT_STARTED'});
    fetch('/api/contact', {
      method: 'POST',
      body: JSON.stringify(form)
    }).then(res => res.json()).then(result => dispatch({type: 'CONTACT_SUCCESS', result}), err => dispatch({type: 'CONTACT_FAIL', err}))
  }
}
