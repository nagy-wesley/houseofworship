import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SongText from 'src/comp/Song/SongText';
import { getSong } from 'src/redux/Songs/action';
import { Helmet } from 'react-helmet';
import _ from 'lodash';

class Song extends Component {
  constructor(props) {
    super(props);
    const { songUrl } = props.match.params;
    this.state = {
      pageTitle: null,
      songUrl,
      selectedSong: {},
    };
  }

  componentWillMount() {
    if (_.isEmpty(this.state.selectedSong)) {
      this.props.getSong(this.state.songUrl);
    } else if (this.state.selectedSong.title !== null) {
      this.setState({ pageTitle: `كوردات ترنيمة ${this.state.selectedSong.title}` });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(nextProps.songs, this.props.songs)) {
      const selectedSong = _.find(nextProps.songs, { uniqueUrl: this.state.songUrl });
      this.setState({ selectedSong });
    }
  }

  componentDidUpdate() {
    const songTitle = _.get(this.state, 'selectedSong.title');
    if (songTitle && !_.includes(this.state.pageTitle, songTitle)) {
      this.setState({ pageTitle: `كوردات ترنيمة ${this.state.selectedSong.title}` }); //eslint-disable-line
    }
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>{this.state.pageTitle}</title>
          <meta name="description" content={`كوردات ترنيمة ${this.state.selectedSong.title}`} />
        </Helmet>
        {
        !_.isEmpty(this.state.selectedSong) ?
          <div key={this.state.selectedSong.id} className="song">
            <h1 className="song-title">
              {this.state.selectedSong.title}
            </h1>
            <SongText lyrics={this.state.selectedSong.content} />
          </div>
          : <p>لم يتم العثور على هذه الترنيمة الرجاء الرجوع الى الصفحة الرئيسية</p>
      }
      </React.Fragment>);
  }
}
Song.propTypes = {
  songs: PropTypes.object.isRequired,
  match: PropTypes.object.isRequired,
  getSong: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({ songs: state.Songs.songs });

export default connect(mapStateToProps, { getSong })(Song);
