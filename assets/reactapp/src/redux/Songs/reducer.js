function loadAllSongs(state, payload) {
  const { songsList } = payload;
  return {
    ...state,
    songsList,
  };
}

function loadSingleSong(state, payload) {
  const { song } = payload;
  const songs = {
    ...state.songs,
    ...song,
  };
  return {
    ...state,
    songs,
  };
}

function saveSongSucces(state) {
  return {
    ...state,
    songsList: [],
    latest: [],
  };
}

function loadHomepageSongs(state, payload) {
  const { songs, listType } = payload;
  return {
    ...state,
    [`${listType}`]: songs,
  };
}


function loadSearchResult(state, payload) {
  const { searchResult } = payload;
  return {
    ...state,
    searchResult,
  };
}

export default {
  LOAD_ALL_SONGS_SUCCESS: loadAllSongs,
  LOAD_SINGLE_SONG_SUCCESS: loadSingleSong,
  SAVING_SONG_SUCCESS: saveSongSucces,
  LOAD_HOMEPAGE_SONGS_SUCCESS: loadHomepageSongs,
  SEARCH_COMPLETED_SUCCESS: loadSearchResult,
};
