import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { Recap } from 'src/comp/misc/Scripts/Recaptcha';
import { Done } from 'src/comp/Pages/ContactUs/Done';
import { sendContactUsMessage } from 'src/redux/Songs/action';
import _ from 'lodash';

class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.verifyCallback = this.verifyCallback.bind(this);
    this.expiredCallback = this.expiredCallback.bind(this);
    this.resetState = this.resetState.bind(this);
  }

  componentWillMount() {
    this.resetState();
  }

  verifyCallback() {
    this.setState({ disabledAdd: false, recaptchaValidated: true });
  }

  expiredCallback() {
    this.setState({ recaptchaValidated: false });
  }


  resetState() {
    this.setState({
      recaptchaValidated: false,
      disabledAdd: true,
      Done: false,
      form: {
        email: '',
        message: '',
      },
      emailError: '',
      messageError: '',
    });
  }

  send() {
    if (!this.state.recaptchaValidated) {
      this.setState({ disabledAdd: true });
      return;
    }

    const emptyEmail = _.isEmpty(this.state.form.email);
    const emptyMessage = _.isEmpty(this.state.form.message);

    (emptyEmail)
      ? this.setState({ emailError: 'ادخل بريدك الإلكتروني' })
      : this.setState({ emailError: '' });

    (emptyMessage)
      ? this.setState({ messageError: 'ادخل الرسالة' })
      : this.setState({ messageError: '' });

    if (!emptyEmail && !emptyMessage) {
      this.props.sendContactUsMessage(this.state.form);
      this.setState({ Done: true });
    }
  }

  handleInput(e, value) {
    const tempForm = {
      ...this.state.form,
      [e.target.name]: value,
    };
    this.setState({ form: tempForm });
  }

  render() {
    return (
      <React.Fragment>
        <Helmet>
          <title>اتصل بنا</title>
          <meta name="description" content="تواصل مع فريق عمل بيت التسبيح" />
        </Helmet>
        <div className="form">
          {
          !this.state.Done
            ? (
              <React.Fragment>
                <div className="form-message">
                  <h3>
                  تواصل مع فريق عمل بيت التسبيح
                  </h3>
                  <p>مشكلة؟ إقتراح؟ نحب أن نسمع منك.</p>
                </div>
                <Paper className="form-paper">
                  <TextField
                    className="right-col"
                    name="email"
                    hintText="بريدك الإلكتروني*"
                    errorText={this.state.emailError}
                    onChange={(e, v) => {
                    this.handleInput(e, v);
                  }}
                  />
                  <TextField
                    className="fullwidth-col"
                    name="message"
                    hintText="الرسالة*"
                    errorText={this.state.messageError}
                    fullWidth
                    onChange={(e, v) => {
                    this.handleInput(e, v);
                  }}
                  />
                </Paper>
                <div className="save-button">
                  <RaisedButton
                    label="ارسل رسالتك"
                    primary={!this.state.disabledAdd}
                    disableTouchRipple={this.state.disabledAdd}
                    onClick={() => {
                    this.send();
                  }}
                  />

                </div>
                <Recap verifyCallback={this.verifyCallback} expiredCallback={this.expiredCallback} />

              </React.Fragment>)
            : <Done />

        }
        </div>
      </React.Fragment>);
  }
}
ContactUs.propTypes = {
  sendContactUsMessage: PropTypes.func.isRequired,
};

export default connect(null, { sendContactUsMessage })(ContactUs);
